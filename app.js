const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const dotenv = require('dotenv').config();
const session = require('express-session');
const { v4: uuidv4 } = require('uuid');
const MySQLStore = require('express-mysql-session')(session);
const db = require('./db');

const sessionStore = new MySQLStore(db.options);

const userRoutes = require('./routes/user');
const adminRoutes = require('./routes/admin');
const indexRoutes = require('./routes/index');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

app.use(session({
    genid: function(req) {
        return uuidv4();
    },
    key: process.env.sessionKey,
    store: sessionStore,
    secret: process.env.sessionSecret,
    resave: false,
    saveUninitialized: true
}));

app.use(userRoutes);
app.use(adminRoutes);
app.use(indexRoutes);

app.all('*', (req, res) => res.send(`The route localhost:3000${req.originalUrl} doesn't exist with ${req.method} method.`));

const PORT = 3000 || process.env.PORT;
app.listen(PORT, () => console.log(`Server running at ${PORT}`));