const express = require('express');
const { parse } = require('uuid');
const router = express.Router();
const executeQuery = require('../db');
const indexObj = require('./index');

router.get('/api/admin', indexObj.isAdminLoggedIn, (req, res) => {
    res.send('This is the admin home page.');
});

// add items
router.post('/api/admin/item', indexObj.isAdminLoggedIn, async (req, res) => {
    try {
        const query = 'INSERT INTO items(name, price) VALUES(?, ?)';
        const params = [req.body.name, req.body.price];
        await executeQuery(query, params);
        res.redirect('/api/items');
    } catch (error) {
        console.log(error);
    }
});

// update an item
router.patch('/api/admin/item/:id', indexObj.isAdminLoggedIn, async (req, res) => {
    try {
        const queryKeys = [];
        const params = [];
        Object.keys(req.body).forEach(key => {
            queryKeys.push(key);
            params.push(req.body[key]);
        });
        var query = 'UPDATE items SET ';
        queryKeys.forEach((key, index) => {
            if(index < queryKeys.length-1)
                query += `${key}=?, `;
            else
                query += `${key}=? `;
        });
        query += 'WHERE id=?';
        params.push(parseInt(req.params.id));
        const result = await executeQuery(query, params);
        if(!result.affectedRows)
            return res.send('Item not found.');
        return res.redirect('/api/items');
    } catch (error) {
        console.log(error);
    }
});

// delete an item
router.delete('/api/admin/item/:id', indexObj.isAdminLoggedIn, async (req, res) => {
    try {
        const query = 'DELETE FROM items WHERE id=?';
        const params = [parseInt(req.params.id)];
        const result = await executeQuery(query, params);
        if(!result.affectedRows)
            return res.send('Item not found.');
        return res.redirect('/api/items');
    } catch (error) {
        console.log(error);
    }
});

// get all orders which are in pending state
router.get('/api/admin/orders', indexObj.isAdminLoggedIn, async (req, res) => {
    try {
        const query = 'SELECT orders.order_id, orders.user_id, users.email, orders.address, orders.phone_no, orders.order_date, order_details.item_id, items.name, order_details.quantity, order_details.price, order_details.payment_method, order_details.status, order_details.delivered_at FROM orders JOIN order_details ON orders.order_id=order_details.order_id JOIN users ON orders.user_id=users.id JOIN items ON order_details.item_id=items.id WHERE order_details.status!=?';
        const params = [ 'cancelled' ];
        const orderItems = await executeQuery(query, params);
        res.send(orderItems);
    } catch (error) {
        console.log(error);   
    }
});

// confirm an order
router.post('/api/admin/orders/:order_id/items/:item_id/confirm', indexObj.isAdminLoggedIn, async(req, res) => {
    try {
        const query = 'UPDATE order_details SET status=? WHERE order_id=? AND item_id=? AND status=?';
        const params = [ 'confirmed', req.params.order_id, parseInt(req.params.item_id), 'pending' ];
        const result = await executeQuery(query, params);
        if(result.affectedRows)
            return res.send('The order has been confirmed.');
        return res.send('The order could not be confirmed.');
    } catch (error) {
        console.log(error);
    }
});

// cancel an order
router.post('/api/admin/orders/:order_id/items/:item_id/cancel', indexObj.isAdminLoggedIn, async (req, res) => {
    try {
        const query = 'UPDATE order_details SET status=? WHERE order_id=? AND item_id=? AND status=?';
        const params = [ 'cancelled', req.params.order_id, parseInt(req.params.item_id), 'pending' ];
        const result = await executeQuery(query, params);
        if(result.affectedRows)
            return res.send('The order has been cancelled.');
        return res.send('The order could not be cancelled.');
    } catch (error) {
        console.log(error);
    }
});

module.exports = router;