const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const executeQuery = require('../db');

const isUserLoggedIn = async (req, res, next) => {
    if(req.session.user_id){
        if(req.params.user_id){
            if(parseInt(req.params.user_id) != req.session.user_id)
                return res.send(`You aren't authorized to access this route.`);
        }
        try {
            const query = 'SELECT type FROM users WHERE id=?';
            const params = [parseInt(req.session.user_id)];
            const userType = await executeQuery(query, params);
            if(userType[0].type == 'user')
                return next();
            return res.send('Admin cannot be granted the request.');
        } catch (error) {
            console.log(error);
        }
    } else {
        res.send('Please log in first.');
    }
}

const isAdminLoggedIn = async (req, res, next) => {
    if(req.session.user_id){
        try {
            const query = 'SELECT type FROM users WHERE id=?';
            const params = [parseInt(req.session.user_id)];
            const userType = await executeQuery(query, params);
            if(userType[0].type == 'admin')
                return next();
            return res.send('User cannot be granted the request.');
        } catch (error) {
            console.log(error);
        }
    } else {
        res.send('Please log in first.');
    }
}

const mapUserToCart = async (req, res) => {
    try {
        // querying to find out whether the logged in user already has a cart
        var query = 'SELECT * FROM cart WHERE user_id=?';
        var params = [req.session.user_id];
        const userCart = await executeQuery(query, params);
        if(userCart.length){ // if the user has a cart
            // querying to find all the newly added items in his cart
            query = 'SELECT * FROM cart_items WHERE cart_id=?';
            params = [parseInt(req.cookies['cart_id'])];
            const userCartItemsNew = await executeQuery(query, params);
            userCartItemsNew.forEach(async(item) => {
                // checking if some or all of the newly added items already exist in the user's cart
                query = 'SELECT * FROM cart_items WHERE cart_id=? AND item_id=?';
                params = [userCart[0].id, item.item_id];
                const userCartItemPrev = await executeQuery(query, params);
                if(userCartItemPrev.length){ // if they do, we update the quantity and price
                    query = 'UPDATE cart_items SET quantity=?, price=? WHERE cart_id=? AND item_id=?';
                    params = [
                        item.quantity + userCartItemPrev[0].quantity,
                        item.price + userCartItemPrev[0].price,
                        userCart[0].id,
                        item.item_id
                    ];
                    await executeQuery(query, params);
                } else { // if they dont, we update the cart id with the user's actual cart id
                    query = 'UPDATE cart_items SET cart_id=? WHERE cart_id=? AND item_id=?';
                    params = [ userCart[0].id, item.cart_id, item.item_id ];
                    await executeQuery(query, params);
                }
                // deleting the entry from cart corresponding to the cart id stored in the cookie
                query = 'DELETE FROM cart WHERE id=?';
                params = [ parseInt(req.cookies['cart_id']) ];
                await executeQuery(query, params);
            });
        } else { // if the user doesn't have an existing cart cart, we map the new cart entry with the current user
            query = 'UPDATE cart SET user_id=? WHERE id=?';
            params = [ req.session.user_id, parseInt(req.cookies['cart_id']) ];
            await executeQuery(query, params);
        }
    } catch (error) {
        console.log(error);
    }
}

router.post('/api/login', async (req, res) => {
    try {
        if(req.session.user_id)
            return res.send('You are already logged in.');
        const query = 'SELECT * FROM users WHERE email=?';
        const params = [req.body.email];
        const user = await executeQuery(query, params);
        if(user.length){
            const validation = await bcrypt.compare(req.body.password, user[0].password);
            if(validation){
                req.session.user_id = user[0].id;
                if(user[0].type == 'user'){
                    if(typeof(req.cookies['cart_id']) !== 'undefined')
                        mapUserToCart(req, res);
                    res.clearCookie('cart_id');
                    return res.send('User login successful.');
                }
                return res.send('Admin login Successful.');
            } else {
                res.send('The password is incorrect.');
            }
        }
        res.send('The user does not exist.');
    } catch (error) {
        console.log(error);
    }
});

router.post('/api/register', async (req, res) => {
    try {
        const passwordHash = await bcrypt.hash(req.body.password, 12);
        const user = {
            email: req.body.email,
            type: 'user',
            password: passwordHash
        };
        const query = 'INSERT INTO users SET ?';
        const params = [user];
        await executeQuery(query, params);
        res.send('Successful Registration.');
    } catch (error) {
        if(error.code == 'ER_DUP_ENTRY')
            res.send('A user with the provided email id already exists.');
    }
});

router.post('/api/logout', (req, res) => {
    if(!req.session.user_id)
        return res.send('Log in first.')
    req.session.user_id = null;
    req.session.id = null;
    return res.send('You have been logged out.');
});

module.exports = router;
module.exports.isUserLoggedIn = isUserLoggedIn;
module.exports.isAdminLoggedIn = isAdminLoggedIn;
// module.exports.mapUserToCart = mapUserToCart;