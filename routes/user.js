const { parse } = require('dotenv');
const express = require('express');
const router = express.Router();
const executeQuery = require('../db');
const indexObj = require('./index');
const { v4: uuidv4 } = require('uuid');
const { param, route } = require('./index');

const insertItemIntoCart_Items = async (req, cartID, price) => {
    const query = 'INSERT INTO cart_items(cart_id, item_id, quantity, price) VALUES(?, ?, ?, ?)';
    const params = [ cartID, parseInt(req.params.id), req.body.quantity, price * req.body.quantity ];
    await executeQuery(query, params);
}

const findItemFromCart_Items = async (req, cartID) => {
    const query = 'SELECT * FROM cart_items WHERE cart_id=? AND item_id=?';
    const params = [ cartID, parseInt(req.params.id) ];
    const item = await executeQuery(query, params);
    return item;
}

const updateItemFromCart_Items = async (req, cartItem, price, cartID) => {
    const query = 'UPDATE cart_items SET quantity=?, price=? WHERE cart_id=? AND item_id=?';
    const params = [
        cartItem.quantity + req.body.quantity,
        price * (cartItem.quantity + req.body.quantity),
        cartID,
        parseInt(req.params.id)
    ];
    await executeQuery(query, params);
}

// show all items in the database
router.get('/api/items', async (req, res) => {
    try {
        const query = 'SELECT * FROM items';
        const params = [];
        const items = await executeQuery(query, params);
        if(items.length)
            return res.send(items);
        return res.send('Currently there is no item in the database to display.');
    } catch (error) {
        console.log(error);
    }
});

// show details about a specific item
router.get('/api/items/:id', async (req, res) => {
    try {
        const query = 'SELECT * FROM items WHERE id=?';
        const params = [parseInt(req.params.id)];
        const item = await executeQuery(query, params);
        if(!item.length)
            return res.send('No item found.');
        return res.send(item);
    } catch (error) {
        console.log(error);
    }
});

// add an item to a user's cart
router.post('/api/items/:id/addtocart', async (req, res) => {
    try {
        // finding the price of the item to be added to the cart
        var query = 'SELECT id, price FROM items WHERE id=?';
        var params = [parseInt(req.params.id)];
        const item = await executeQuery(query, params);
        var price = item[0].price;
        // if user is logged in
        if(req.session.user_id){
            // querying to find out whether the logged in user already has a cart
            query = 'SELECT * FROM cart WHERE user_id=?';
            params = [req.session.user_id];
            const userCart = await executeQuery(query, params);
            if (userCart.length) { // if the user has a cart
                const cartID = userCart[0].id;
                const cartItem = await findItemFromCart_Items(req, cartID);
                if (cartItem.length) { // if the item to be added to cart already exists in the user's cart
                    await updateItemFromCart_Items(req, cartItem[0], price, cartID);
                    return res.send('Cart updated.');
                } else { // if the user adding a new item to his cart
                    await insertItemIntoCart_Items(req, cartID, price);
                    return res.send('Item added to cart.');
                }
            } else { // if the user doesnt have a cart
                // creating a new entry in the cart table for the logged in user
                query = 'INSERT INTO cart(user_id) VALUES(?)';
                params = [ req.session.user_id ];
                const cart = await executeQuery(query, params);
                const cartID = cart.insertId;
                // inserting the item information into the user-cart table and mapping the corresponding cart entry
                await insertItemIntoCart_Items(req, cartID, price);
                return res.send('Item added to cart.');
            }
        }
        // if user is not logged in
        if(typeof(req.cookies['cart_id']) === 'undefined') { // if no cart id exists in the cookie
            // inserting a row into the cart table with user id as null
            query = 'INSERT INTO cart() VALUES ()';
            params = [];
            const cart = await executeQuery(query, params);
            const cartID = cart.insertId;
            // creating a cookie to store the cart id
            res.cookie('cart_id', cartID, {
                httpOnly: true
            });
            // inserting the item information into the user-cart table and mapping the corresponding cart entry
            await insertItemIntoCart_Items(req, cartID, price);
            return res.send('Item added to cart.');
        } else { // if cart id exists in the cookie
            const cartID = parseInt(req.cookies['cart_id']);
            // finding out whether the item to be added already exists in the user-cart with the cart id same as in the cookie
            const cartItem = await findItemFromCart_Items(req, cartID);
            if(cartItem.length){ // if the item exists, we update the quantity and price
                await updateItemFromCart_Items(req, cartItem[0], price, cartID);
                return res.send('Cart updated.');
            } else { // else we create a new entry in user-cart table and store the item information
                await insertItemIntoCart_Items(req, cartID, price);
                return res.send('Item added to cart.');
            }
        }
    } catch (error) {
        console.log(error);
    }
});

// show a user's cart
router.get('/api/users/:user_id/cart', indexObj.isUserLoggedIn, async (req, res) => {
    try {
        const query = 'SELECT cart_items.item_id, items.name, cart_items.quantity, cart_items.price FROM users JOIN cart ON users.id=cart.user_id JOIN cart_items on cart.id=cart_items.cart_id JOIN items ON cart_items.item_id=items.id WHERE cart.user_id=?';
        const params = [ req.session.user_id ];
        const results = await executeQuery(query, params);
        if(results.length)
            return res.send(results);
        return res.send('Your cart is empty.');
    } catch (error) {
        console.log(error);
    }
});

// update an item in a user's cart
router.patch('/api/users/:user_id/cart/:cart_id/cartitems/:item_id', indexObj.isUserLoggedIn, async (req, res) => {
    try {
        var query = 'SELECT * FROM items WHERE id=?';
        var params = [parseInt(req.params.item_id)];
        const item = await executeQuery(query, params);
        query = 'SELECT * FROM cart WHERE user_id=?';
        params = [ req.session.user_id ];
        const cart = await executeQuery(query, params);
        if(cart.length){
            if(cart[0].id !== parseInt(req.params.cart_id))
                return res.send('You are not authorized.');
            query = 'UPDATE cart_items SET quantity=?, price=? WHERE cart_id=? AND item_id=?';
            params = [ req.body.quantity, item[0].price * req.body.quantity, cart[0].id, parseInt(req.params.item_id) ];
            const result = await executeQuery(query, params);
            if(result.affectedRows)
                return res.send('Cart updated.');
            return res.send('Could not update your cart.');
        }
        res.send('You are not authorized.');
    } catch (error) {
        console.log(error);
    }
});

// delete an item from a user's cart
router.delete('/api/users/:user_id/cart/:cart_id/cartitems/:item_id', indexObj.isUserLoggedIn, async (req, res) => {
    try {
        var query = 'SELECT * FROM cart WHERE user_id=?';
        var params = [ req.session.user_id ];
        const cart = await executeQuery(query, params);
        if(cart.length){
            if(cart[0].id !== parseInt(req.params.cart_id))
                return res.send('You are not authorized.');
            query = 'DELETE FROM cart_items WHERE cart_id=? AND item_id=?';
            params = [ cart[0].id, parseInt(req.params.item_id) ];
            const result = await executeQuery(query, params);
            query = 'SELECT * FROM cart_items WHERE cart_id=?';
            params = [cart[0].id];
            const items = await executeQuery(query, params);
            if(!items.length){
                query = 'DELETE FROM cart WHERE id=?';
                params = [cart[0].id];
                await executeQuery(query, params);
            }
            if(result.affectedRows)
                return res.send('Item deleted from your cart.');
            return res.send('Could not delete item from your cart.');
        }
        res.send('You are not authorized.');
    } catch (error) {
        console.log(error);
    }
});

// place an order for a user
router.post('/api/users/:user_id/checkout', indexObj.isUserLoggedIn, async (req, res) => {
    try {
        var query = 'SELECT cart_items.cart_id, cart_items.item_id, cart_items.quantity, cart_items.price FROM users JOIN cart ON users.id=cart.user_id JOIN cart_items on cart.id=cart_items.cart_id JOIN items ON cart_items.item_id=items.id WHERE cart.user_id=?';
        var params = [ req.session.user_id ];
        const cartItems = await executeQuery(query, params);
        var cartTotal = 0;
        cartItems.forEach(item => cartTotal += item.price);
        query = 'INSERT INTO orders(order_id, user_id, address, phone_no, total_price) VALUES (?, ?, ?, ?, ?)';
        const uniqueID = uuidv4();
        params = [
            uniqueID,
            req.session.user_id,
            req.body.address,
            req.body.phone,
            cartTotal
        ];
        await executeQuery(query, params);
        // transaction
        query = 'INSERT INTO order_details(order_id, item_id, quantity, price) values';
        cartItems.forEach((item, index) => {
            if(index < cartItems.length-1)
                query += ' (?, ?, ?, ?),';
            else
                query += ' (?, ?, ?, ?);';
        });
        params = [];
        cartItems.forEach(item => {
            params.push(uniqueID),
            params.push(item.item_id);
            params.push(item.quantity);
            params.push(item.price);
        });
        await executeQuery(query, params);
        query = 'DELETE FROM cart WHERE id=?';
        params = [ cartItems[0].cart_id ];
        await executeQuery(query, params);
        res.send('Oder placed successfully.');
    } catch (error) {
        console.log(error);
    }
});

// show a user's all orders
router.get('/api/users/:user_id/orders', indexObj.isUserLoggedIn, async (req, res) => {
    try {
        var query = 'SELECT orders.order_id, orders.address, orders.phone_no, order_details.item_id, order_details.quantity, order_details.price, order_details.status FROM users JOIN orders ON users.id=orders.user_id JOIN order_details on orders.order_id=order_details.order_id where orders.user_id=?';
        var params = [ req.session.user_id ];
        const orders = await executeQuery(query, params);
        if(orders.length)
            return res.send(orders);
        return res.send('Your order list is empty.');
    } catch (error) {
        console.log(error);
    }
});

// cancel an order
router.post('/api/users/:user_id/orders/:order_id/items/:item_id/cancel', indexObj.isUserLoggedIn, async (req, res) => {
    try {
        var query = 'SELECT * FROM orders WHERE order_id=? AND user_id=?';
        var params = [ req.params.order_id, req.session.user_id ];
        const order = await executeQuery(query, params);
        if(order.length){
            query = 'UPDATE order_details SET status=? WHERE order_id=? AND item_id=? and status=?';
            params = [ 'cancelled', req.params.order_id, req.params.item_id, 'pending' ];
            const result = await executeQuery(query, params);
            if(result.affectedRows)
                return res.send('Your item has been cancelled.');
            return res.send('The order could not be cancelled.');
        }
        return res.send('You are not authorized.');
    } catch (error) {
        console.log(error);
    }
});

module.exports = router;