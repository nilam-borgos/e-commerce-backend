const mysql = require('mysql');
const dotenv = require('dotenv').config();

const options = {
    host: 'localhost',
    user: process.env.user,
    password: process.env.password,
    database: 'ecomm_backend'
};

const connection = mysql.createConnection(options);

const executeQuery = (query, params) => {
    return new Promise((resolve, reject) => {
        connection.query(query, params, (error, result) => {
            if(error)
                reject(error);
            resolve(result);
        });
    });
};

module.exports = executeQuery;
module.exports.options = options;