create database ecomm_backend;
use ecomm_backend;

create table users(
    id int not null auto_increment primary key,
    email varchar(30) not null unique,
    type varchar(10) not null,
    password varchar(100) not null
);

create table items(
    id int not null auto_increment primary key,
    name varchar(20) not null,
    price int not null
);

create table cart(
    id int not null auto_increment primary key,
    user_id int,
    foreign key (user_id) references users(id)
);

create table cart_items(
    cart_id int not null,
    item_id int not null,
    quantity int not null,
    price int not null,
    primary key(cart_id, item_id),
    foreign key (cart_id) references cart(id) on delete cascade,
    foreign key (item_id) references items(id) on delete cascade
);

create table orders(
    order_id varchar(50) not null primary key,
    user_id int not null,
    address varchar(200),
    phone_no varchar(10),
    total_price int not null,
    order_date timestamp default now(),
    foreign key (user_id) references users(id)
);

create table order_details(
    order_id varchar(50) not null,
    item_id int not null,
    quantity int not null,
    price int not null,
    payment_method varchar(20) default 'cod',
    status varchar(10) default 'pending',
    is_paid boolean default false,
    delivered_at timestamp default null,
    primary key(order_id, item_id),
    foreign key (order_id) references orders(order_id) on delete cascade,
    foreign key (item_id) references items(id) on delete cascade
);